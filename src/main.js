import Vue from 'vue'
import App from './App.vue'

import router from './router'
import store from './store'

Vue.config.productionTip = false

import('bootstrap/dist/css/bootstrap.css')

// vue 3
// const app = createApp(App)
//
// app.use(router)
// app.use(store)
//
// app.mount('#app')
// --

new Vue({
  data:{},
  router,
  store,
  render: h => h(App),
}).$mount('#qqq')
