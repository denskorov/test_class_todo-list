import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import HomePage from "@/pages/HomePage.vue";
import TodoPage from "@/pages/TodoPage.vue";
import Page404 from "@/pages/404Page.vue";

// const router = createRouter({
//     history: createWebHashHistory(),
//     routers: []
// })


// vue 2
const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage
        },
        {
            path: '/todo/:id',
            name: 'todo',
            component: TodoPage
        },
        {
            path: '*',
            name: '404',
            component: Page404
        },
    ]
})

export default router