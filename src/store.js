//vue 2
import Vue from 'vue'
import Vuex from 'vuex'

// vue 3
// import {createStore} from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({

    state: () => ({
        todos: [],
        newTodoTitle: ''
    }), // 0
    mutations: {
        setTodos: (state, todos) => state.todos = todos,
        setNewTodoTitle: (state, title) => state.newTodoTitle = title,
    }, // 1
    getters: {
        count: (state) => state.todos.filter(t => !t.isDeleted).length,
        countCompleted: (state) => state.todos.filter(todo => todo.completed && !todo.isDeleted).length,
        countUncompleted: (state) => state.todos.filter(todo => !todo.completed && !todo.isDeleted).length,
    }, // 2
    actions: {
        addTodo({state, commit}) {
            if (state.newTodoTitle.trim() === '') {
                commit('setNewTodoTitle', '')
                return
            }

            const todo = {
                id: +(new Date()),
                title: state.newTodoTitle,
                completed: false
            }

            commit('setTodos', [todo, ...state.todos])
            // this.todos.push(todo)
            commit('setNewTodoTitle', '')
            // this.newTodoTitle = ''
            // this.$refs.inputTitle.focus() // document.qu....('....')

            localStorage.setItem('todos', JSON.stringify(state.todos))
        },
        removeTodo({state, commit}, id) {
            return new Promise((resolve) => {
                commit('setTodos', state.todos.filter(todo => todo.id !== id))
                resolve(state.todos)
            })
        },
        changeCompleted({state, commit}, [item, isChecked]) {
            const newTodos = state.todos.map(t => {
                if (item.id === t.id) {
                    return {
                        ...t,
                        completed: isChecked
                    }
                }
                return t
            })
            commit('setTodos', newTodos)
            localStorage.setItem('todos', JSON.stringify(state.todos))
        },
        changeTitle({state, commit}, [item, title]) {
            const newTodos = state.todos.map(t => {
                if (item.id === t.id) {
                    return {
                        ...t,
                        title: title
                    }
                }
                return t
            })
            commit('setTodos', newTodos)
            localStorage.setItem('todos', JSON.stringify(state.todos))
        }
    } // 3

})

export default store
